﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Przewodnik.Resources;
using System.IO.IsolatedStorage;

namespace Przewodnik
{
    public partial class MainPage : PhoneApplicationPage
    {
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }


        private void ListBasicPage_Click(object sender, RoutedEventArgs e)
        {
            if (!settings.Contains("sorted"))
            {
                settings.Add("sorted", false);
            }
            else
            {
                settings["sorted"] = false;
            }
            settings.Save();
            NavigationService.Navigate(new Uri("/Views/ListBasicPage.xaml", UriKind.Relative));
        }

        private void ListFavouritePage_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/ListFavouritePage.xaml", UriKind.Relative));
        }

        private void ListNearPage_Click(object sender, RoutedEventArgs e)
        {
            if (!settings.Contains("sorted"))
            {
                settings.Add("sorted", true);
            }
            else
            {
                settings["sorted"] = true;
            }
            settings.Save();
            NavigationService.Navigate(new Uri("/Views/ListBasicPage.xaml", UriKind.Relative));

        }

        private void MapPage_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/MapPage.xaml", UriKind.Relative));
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}