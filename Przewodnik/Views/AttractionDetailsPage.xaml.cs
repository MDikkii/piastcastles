﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Przewodnik.DataServices;
using System.Threading.Tasks;
using System.ComponentModel;
using Przewodnik.DataModels;
using Microsoft.Phone.Tasks;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Device.Location;

namespace Przewodnik.Views
{
    public partial class AttractionDetailsPage : PhoneApplicationPage, INotifyPropertyChanged
    {
        AttractionListService attractionService;
        private AttractionModel Attraction;
        private bool favList;

        public AttractionDetailsPage()
        {
            InitializeComponent();  
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            await Initialize();
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            attractionService = null;

        }

        public async Task Initialize()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            try
            {
                favList = (bool)settings["favlist"];
            }catch(Exception ex){}
            if (!settings.Contains("favlist"))
            {
                settings.Add("favlist", false);
            }
            else
            {
                settings["favlist"] = false;
            }
            settings.Save();

            attractionService = AttractionListService.Instance;

            if (attractionService.AttractionList.Items.Count == 0)
                await attractionService.InitializeAtrakcjeList();

            if(favList)
                Attraction = attractionService.FavouriteAttractions.ElementAt(attractionService.FavSelectedIndex);
            else
                Attraction = attractionService.AttractionList.Items.ElementAt(attractionService.SelectedIndex);

            LayoutRoot.DataContext = Attraction;

            ApplicationBarIconButton favBtn = (ApplicationBarIconButton)ApplicationBar.Buttons[3];
            if (Attraction.Favourite)
                favBtn.IconUri = new Uri("/Assets/Icons/minus_favs.png", UriKind.RelativeOrAbsolute);
            else
                favBtn.IconUri = new Uri("/Assets/Icons/addto_favs.png", UriKind.RelativeOrAbsolute);

            ApplicationBarIconButton callBtn = (ApplicationBarIconButton)ApplicationBar.Buttons[2];
            if (Attraction.PhoneNumber == 0)
                callBtn.IsEnabled = false;
            else
                callBtn.IsEnabled = true;
            

        }

        public event PropertyChangedEventHandler PropertyChanged;
        

        public void NotifyPropertyChanged(string propertyName)
        {          
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Navigate_BarButton(object sender, EventArgs e)
        {

            MapsDirectionsTask mapsDirectionsTask = new MapsDirectionsTask();

            GeoCoordinate endPoint = new GeoCoordinate(Attraction.Latitude, Attraction.Longitude);
            LabeledMapLocation endPointLabel = new LabeledMapLocation(Attraction.Name, endPoint);

            mapsDirectionsTask.End = endPointLabel;

            mapsDirectionsTask.Show();

            

        }

        public void Call_BarButton(object sender, EventArgs e)
        {

                PhoneCallTask phoneCallTask = new PhoneCallTask();

                phoneCallTask.PhoneNumber = Attraction.PhoneNumber.ToString();
                phoneCallTask.DisplayName = Attraction.Name;

                phoneCallTask.Show();

        }

        public void Website_BarButton(object sender, EventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri(Attraction.Website, UriKind.Absolute);

            webBrowserTask.Show();
        }

        public void Fav_BarButton(object sender, EventArgs e)
        {
            if (Attraction.Favourite)
            {
                Attraction.Favourite = false;
                attractionService.AttractionList.Items.ElementAt(attractionService.SelectedIndex).Favourite = false;
                RemoveFromFav(attractionService.AttractionList.Items.ElementAt(attractionService.SelectedIndex).Name);
                Debug.WriteLine(attractionService.FavouriteAttractions.Count);
            }
            else
            {
                Attraction.Favourite = true;
                attractionService.AttractionList.Items.ElementAt(attractionService.SelectedIndex).Favourite = true;
                attractionService.FavouriteAttractions.Add(new AttractionModel(Attraction));
            }
            ApplicationBarIconButton button = (ApplicationBarIconButton)ApplicationBar.Buttons[3];
            if (Attraction.Favourite)
                button.IconUri = new Uri("/Assets/Icons/minus_favs.png", UriKind.RelativeOrAbsolute);
            else
                button.IconUri = new Uri("/Assets/Icons/addto_favs.png", UriKind.RelativeOrAbsolute);
        }

        public void RemoveFromFav(string name)
        {
            int x = -1;
            int i = 0;
            while(x==-1 && i < attractionService.FavouriteAttractions.Count())
            {
                var am = attractionService.FavouriteAttractions.ElementAt(i);
                if (am.Name.Equals(name))
                {
                    x = attractionService.FavouriteAttractions.IndexOf(am);
                }
                i++;
            }
            attractionService.FavouriteAttractions.Remove(attractionService.FavouriteAttractions.ElementAt(x));
        }
    }
}