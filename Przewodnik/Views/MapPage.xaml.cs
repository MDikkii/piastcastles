﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using Przewodnik.DataServices;
using Przewodnik.DataModels;
using System.Device.Location;
using System.Collections.ObjectModel;
using Microsoft.Phone.Maps.Toolkit;
using Windows.Devices.Geolocation;
using Windows.UI;
using System.Windows.Media;
using System.Diagnostics;
using Microsoft.Phone.Maps.Controls;
using Telerik.Windows.Controls;

namespace Przewodnik.Views
{
    public partial class MapPage : PhoneApplicationPage
    {
        AttractionListService attractionService;
        private Geolocator geolocator;
        public AttractionObservable AttractionList { get; set; }
        public List<MapAttractionModel> PushpinsList { get; set; }
        public GeoCoordinate UserPosition { get; set; }
        UserLocationMarker marker;


        public MapPage()
        {
            InitializeComponent();
            
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            await Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            geolocator.PositionChanged -= geolocator_PositionChanged;
        }

       
        public async Task Initialize()
        {
            attractionService = AttractionListService.Instance;

            if (attractionService.AttractionList.Items.Count == 0)
                await attractionService.InitializeAtrakcjeList();

            AttractionList = attractionService.AttractionList.Items;
            PushpinsList = new List<MapAttractionModel>();

            foreach(AttractionModel am in AttractionList)
            {
                PushpinsList.Add(new MapAttractionModel() { Coordinate = new GeoCoordinate(am.Latitude, am.Longitude), Name = am.Name, City = am.City});
            }

            MapItemsControl PushpinLayer = (MapItemsControl)MapExtensions.GetChildren(AttractionMap).First();
            
            try
            {
                PushpinLayer.ItemsSource = PushpinsList;
            }
            catch(Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            //AttractionItems.ItemsSource = PushpinsList;
            geolocator = new Geolocator();
            geolocator.MovementThreshold = 150;
            geolocator.DesiredAccuracy = PositionAccuracy.High;
            geolocator.PositionChanged += geolocator_PositionChanged;
            if (geolocator.LocationStatus == PositionStatus.Disabled)
            {
                var result = MessageBox.Show("Włącz lokalizację w ustawieniach telefonu, aby zobaczyć swoją pozycję na mapie. \n\nCzy chcesz przejść do ustawień lokalizacji?","Lokalizacja wyłączona",MessageBoxButton.OKCancel);
                if(result == MessageBoxResult.OK)
                {
                    await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
                }
            }
        }


        private void AttractionMap_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "c841a196-29a0-4196-8bcf-a6065e0e20eb";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "y1GzEnUSmSMi-0tR0zbQpQ";

        }
        

        void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            UserPosition = new GeoCoordinate(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);
            Dispatcher.BeginInvoke(() =>
            {
                marker = (UserLocationMarker)this.FindName("UserLocationMarker");
                marker.GeoCoordinate = UserPosition;
            });
        }

        private void RadContextMenuItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            foreach (AttractionModel am in AttractionList)
            {
                if (am.Latitude == p.GeoCoordinate.Latitude && am.Longitude == p.GeoCoordinate.Longitude)
                {
                    attractionService.SelectedIndex = AttractionList.IndexOf(am);
                }
            }
          
            NavigationService.Navigate(new Uri("/Views/AttractionDetailsPage.xaml", UriKind.Relative));
        }
         
        Pushpin p = new Pushpin();

        private void RadContextMenu_Opening(object sender, ContextMenuOpeningEventArgs e)
        {
            p = e.FocusedElement as Pushpin;
            ScaleTransform st =  new ScaleTransform();
            st.ScaleX = 1.25;
            st.ScaleY = 1.25;
            st.CenterX = 25;
            st.CenterY = 82.25;
            
            p.RenderTransform = st;

        }

        private void RadContextMenu_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ScaleTransform st = new ScaleTransform();
            st.ScaleX = 1;
            st.ScaleY = 1;
            p.RenderTransform = st;
        }


      





        

        
    }
}