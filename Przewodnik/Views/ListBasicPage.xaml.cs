﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Xml;
using System.Xml.Serialization;
using Przewodnik.DataModels;
using System.IO.IsolatedStorage;
using System.IO;
using Przewodnik.DataServices;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Devices.Geolocation;
using System.Device.Location;

namespace Przewodnik.Views
{
    public partial class ListBasicPage : PhoneApplicationPage
    {

        private AttractionListService attractionService;
        private bool sortedByDistance
        {
            get;
            set;
        }
        GeoCoordinate UserPosition { get; set; }
        AttractionObservable AttractionList { get; set; }
        private Geolocator geolocator;

        public ListBasicPage()
        {
            InitializeComponent();
            attractionService = AttractionListService.Instance;
        }

        //~ListBasicPage()
        //{ }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            AtrakcjeListBox.SelectedIndex = -1;
            await Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            geolocator.PositionChanged -= geolocator_PositionChanged;
            
        }

        

        public async Task Initialize()
        {
            sortedByDistance = (bool)IsolatedStorageSettings.ApplicationSettings["sorted"];
            //MessageBox.Show(sortedByDistance.ToString());

            if (attractionService.AttractionList.Items.Count == 0 )
                await attractionService.InitializeAtrakcjeList();

            AttractionList = attractionService.AttractionList.Items;
            if (sortedByDistance)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    AttractionList.SortByDistance();
                });
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    AttractionList.SortByName();
                });
            }
            AtrakcjeListBox.ItemsSource = AttractionList;

            

            geolocator = new Geolocator();
            geolocator.MovementThreshold = 150;
            geolocator.DesiredAccuracy = PositionAccuracy.High;
            geolocator.PositionChanged += geolocator_PositionChanged;
            if (geolocator.LocationStatus == PositionStatus.Disabled)
            {
                MessageBox.Show("Włącz lokalizację w ustawieniach telefonu, aby zobaczyć odległości od atrakcji.");
            }
        }

        void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            UserPosition = new GeoCoordinate(args.Position.Coordinate.Latitude,args.Position.Coordinate.Longitude);
            UpdateDistanceToAttraction();
            
        }

        private void UpdateDistanceToAttraction()
        {
            Dispatcher.BeginInvoke(() =>
            {
                foreach (AttractionModel am in AttractionList)
                {
                    am.Distance = Math.Round( UserPosition.GetDistanceTo(new GeoCoordinate(am.Latitude, am.Longitude)) / 1000,2,MidpointRounding.ToEven);
                }
                
            });

            if (sortedByDistance)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    AttractionList.SortByDistance();
                });
            }
            
        }

        private void AtrakcjeListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AtrakcjeListBox.SelectedIndex >= 0 && AtrakcjeListBox.SelectedIndex < attractionService.AttractionList.Items.Count())
            {
                
                AttractionListService.Instance.SelectedIndex = AtrakcjeListBox.SelectedIndex;
                Debug.WriteLine(AttractionListService.Instance.SelectedIndex);
                attractionService.SelectedIndex = AtrakcjeListBox.SelectedIndex;
                NavigationService.Navigate(new Uri("/Views/AttractionDetailsPage.xaml", UriKind.Relative));
            }
            
        }

       


    }
}