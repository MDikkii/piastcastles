﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Przewodnik.DataServices;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using Przewodnik.DataModels;

namespace Przewodnik.Views
{
    public partial class ListFavouritePage : PhoneApplicationPage
    {

        AttractionListService attractionService = AttractionListService.Instance;

        public ListFavouritePage()
        {
            InitializeComponent();         
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            await Initialize();
            AtrakcjeListBox.SelectedIndex = -1;
        }

        public async Task Initialize()
        {
            if (attractionService.AttractionList.Items.Count == 0)
                await attractionService.InitializeAtrakcjeList();

            AtrakcjeListBox.ItemsSource = attractionService.FavouriteAttractions;
        }

        private void AtrakcjeListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AtrakcjeListBox.SelectedIndex >= 0 && AtrakcjeListBox.SelectedIndex < attractionService.AttractionList.Items.Count())
            {
                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                if (!settings.Contains("favlist"))
                {
                    settings.Add("favlist", true);
                }
                else
                {
                    settings["favlist"] = true;
                }
                settings.Save();
                AttractionListService.Instance.FavSelectedIndex = AtrakcjeListBox.SelectedIndex;
                Debug.WriteLine(AttractionListService.Instance.SelectedIndex);
                attractionService.FavSelectedIndex = AtrakcjeListBox.SelectedIndex;
                attractionService.SelectedIndex = FindIndexAttractionList(((AttractionModel)AtrakcjeListBox.SelectedItem).Name);
                AttractionListService.Instance.SelectedIndex = attractionService.SelectedIndex;


                NavigationService.Navigate(new Uri("/Views/AttractionDetailsPage.xaml", UriKind.Relative));
            }
        }

        public int FindIndexAttractionList(string name)
        {
            int x = -1;
            int i = 0;
            while (x == -1 && i < attractionService.AttractionList.Items.Count())
            {
                var am = attractionService.AttractionList.Items.ElementAt(i);
                if (am.Name.Equals(name))
                {
                    x = attractionService.AttractionList.Items.IndexOf(am);
                }
                i++;
            }
            return x;
        }
        
    }
}