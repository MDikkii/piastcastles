﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Przewodnik.DataModels
{
    public class AttractionModel : INotifyPropertyChanged
    {
        public AttractionModel()
        { }

        public AttractionModel(AttractionModel other)
        {
            Distance = other.Distance;
            Latitude = other.Latitude;
            Longitude = other.Longitude;
            Name = other.Name;
            City = other.City;
            PhotoUrl = other.PhotoUrl;
            Description = other.Description;
            Website = other.Website;
            PhoneNumber = other.PhoneNumber;
            Favourite = other.Favourite;
        }

        private double distance;
        private double latitude;
        private double longitude;
        private string name;
        private string city;
        private string photoUrl;
        private string description;
        private string website;
        private int phoneNumber;
        private bool favourite;

        [XmlAttribute("fav")]
        public bool Favourite
        {
            get
            {
                return favourite;
            }
            set
            {
                favourite = value;
                NotifyPropertyChanged("Favourite");
            }
        }

        
        public double Distance
        {
            get
            {
                return distance;
            }
            set
            {
                distance = value;
                NotifyPropertyChanged("Distance");
            }
        }

        [XmlAttribute("lat")]
        public double Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
                NotifyPropertyChanged("Latitude");
            }
        }

        [XmlAttribute("lon")]
        public double Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
                NotifyPropertyChanged("Longitude");
            }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        [XmlAttribute("city")]
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
                NotifyPropertyChanged("City");
            }
        }

        [XmlAttribute("website")]
        public string Website
        {
            get
            {
                return website;
            }
            set
            {
                website = value;
                NotifyPropertyChanged("Website");
            }
        }


        [XmlAttribute("photo_url")]
        public string PhotoUrl
        {
            get
            {
                return photoUrl;
            }
            set
            {
                photoUrl = value;
                NotifyPropertyChanged("PhotoUrl");
            }
        }

        [XmlAttribute("desc")]
        public string Description 
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                NotifyPropertyChanged("Description");
            }
        }

        [XmlAttribute("phone")]
        public int PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
                NotifyPropertyChanged("PhoneNumber");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }



        
    }
}
