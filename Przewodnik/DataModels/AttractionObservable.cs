﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Przewodnik.DataModels
{
    public class AttractionObservable : ObservableCollection<AttractionModel>
    {
        public AttractionObservable()
            : base()
        {
            
        }


        bool suppress = false;
        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!suppress)
            {
                base.OnCollectionChanged(e);
            }
        }


        protected override void MoveItem(int p1, int p2)
        {
            //suppress = true;
            base.MoveItem(p1, p2);
            //suppress = false;
            //OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(
            //System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }

        public void SortByDistance()
        {
            suppress = true;
            int size = this.Count();
            do
            {
                for (int i = 0; i < size - 1; i++)
                {
                    if (this.ElementAt(i).Distance > this.ElementAt(i + 1).Distance)
                    {
                        this.Move(i, i + 1);
                    }
                }
                size--;
            } while (size > 1);
            suppress = false;

            OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(
            System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }

        public void SortByName()
        {
            suppress = true;
            int size = this.Count();
            do
            {
                for (int i = 0; i < size - 1; i++)
                {
                    if (this.ElementAt(i).Name.CompareTo(this.ElementAt(i + 1).Name) > 0)
                    {
                        this.Move(i, i + 1);
                    }
                }
                size--;
            } while (size > 1);
            suppress = false;

            OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(
            System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }
    }

    
   
}
