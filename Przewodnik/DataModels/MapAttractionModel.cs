﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Przewodnik.DataModels
{
    public class MapAttractionModel
    {
        public GeoCoordinate Coordinate { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}
