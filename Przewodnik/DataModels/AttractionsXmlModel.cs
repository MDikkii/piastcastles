﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Przewodnik.DataModels
{
    [XmlRoot("lista_atrakcji")]
    public class AttractionsXmlModel
    {
        [XmlElement("atrakcja")]
        public AttractionObservable Items { get; set; }

        public AttractionsXmlModel()
        {
            Items = new AttractionObservable();
            
        }
    }

    
}
