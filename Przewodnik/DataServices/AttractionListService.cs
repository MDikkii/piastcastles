﻿using Przewodnik.DataModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Przewodnik.DataServices
{
    public sealed class AttractionListService
    {
        private static AttractionListService instance = null;
        private static readonly object padLock = new object();
        public AttractionsXmlModel AttractionList { get; set; }
        public AttractionObservable FavouriteAttractions { get; set; }
        XmlSerializer xmlSer;
        public int SelectedIndex { get; set; }
        public int FavSelectedIndex { get; set; }

        public static AttractionListService Instance
        {
            get
            {
                lock (padLock)
                {
                    if (instance == null)
                    {
                        instance = new AttractionListService();
                    }
                    return instance;
                }
            }
        }

        private AttractionListService()
        {
            Debug.WriteLine("Constructor fire");
            AttractionList = new AttractionsXmlModel();
            xmlSer = new XmlSerializer(typeof(AttractionsXmlModel));
            
        }

        public async Task InitializeAtrakcjeList()
        {
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (myIsolatedStorage.FileExists("attraction.xml"))
                {
                    try
                    {
                        using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("attraction.xml", FileMode.Open))
                        {
                            string xmlString;
                            using (StreamReader sr = new StreamReader(stream))
                            {
                                xmlString = await sr.ReadToEndAsync();
                                using (TextReader reader = new StringReader(xmlString))
                                {
                                    AttractionList = (AttractionsXmlModel)xmlSer.Deserialize(reader);
                                }
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex is FileNotFoundException)
                        {
                            throw new FileNotFoundException("There is no data file", ex);
                        }
                        if (ex is InvalidOperationException)
                        {
                            throw new FileNotFoundException("There are errors in data file", ex);
                        }

                        throw new FileNotFoundException("Unknow exception", ex);
                    }

                    if (AttractionList.Items.Count == 0)
                    {
                        await readFromFile();
                    }
                }
                else
                {
                    await readFromFile();
                }

                FavouriteAttractions = new AttractionObservable();
                foreach (AttractionModel am in AttractionList.Items)
                {
                    if (am.Favourite)
                        FavouriteAttractions.Add(new AttractionModel(am));
                }
                Debug.WriteLine("AtractionList count: {0}, FavAttra count: {1}", AttractionList.Items.Count, FavouriteAttractions.Count);

            }

        }

        private async Task readFromFile()
        {
            try
            {
                string xmlString;
                using (StreamReader sr = new StreamReader("Data/attraction.xml"))
                {
                    xmlString = await sr.ReadToEndAsync();
                    using (TextReader reader = new StringReader(xmlString))
                    {
                        AttractionList = (AttractionsXmlModel)xmlSer.Deserialize(reader);
                    }
                }
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("attraction.xml", FileMode.Create))
                    {
                        using (StreamWriter sw = new StreamWriter(stream))
                        {
                            await sw.WriteAsync(xmlString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is FileNotFoundException)
                {
                    throw new FileNotFoundException("There is no data file", ex);
                }
                if (ex is InvalidOperationException)
                {
                    throw new InvalidOperationException("There are errors in data file", ex);
                }

                throw new Exception("Unknow exception", ex);
            }
        }

        public void SaveAttractions()
        {
            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("attraction.xml", FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        xmlSer.Serialize(sw, AttractionList);
                    }
                }
            }
        }
    }
}
